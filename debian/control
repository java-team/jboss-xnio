Source: jboss-xnio
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Markus Koschany <apo@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk-headless (>= 2:1.8),
 default-jdk-doc,
 junit4,
 libjboss-logging-java,
 libjboss-logging-tools-java,
 libjboss-logmanager-java,
 libjboss-threads-java,
 libjmock2-java,
 libmaven-bundle-plugin-java,
 libmaven-javadoc-plugin-java,
 libsurefire-java,
 libwildfly-client-config-java,
 libwildfly-common-java,
 maven-debian-helper (>= 1.5)
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/java-team/jboss-xnio.git
Vcs-Browser: https://salsa.debian.org/java-team/jboss-xnio
Homepage: https://xnio.jboss.org/

Package: libjboss-xnio-java
Multi-Arch: foreign
Architecture: all
Depends:
 ${maven:Depends},
 ${misc:Depends}
Suggests:
 libjboss-xnio-java-doc,
 ${maven:OptionalDepends}
Description: simplified low-level I/O layer for NIO
 XNIO is a simplified low-level I/O layer which can be used anywhere you are
 using non-blocking I/O today. It frees you from the hassle of dealing with
 Selectors and the lack of NIO support for multicast sockets and non-socket
 I/O, while still maintaining all the capabilities present in NIO, and it opens
 the door to non-obvious optimizations.
 .
 XNIO provides a unique API for combining blocking and non-blocking operations,
 even on the same channel, allowing you to take advantage of the simplicity and
 low latency of blocking I/O while still gaining the thread-conservative and
 throughput benefits of non-blocking I/O.

Package: libjboss-xnio-java-doc
Architecture: all
Section: doc
Multi-Arch: foreign
Depends:
 ${misc:Depends}
Recommends:
 ${maven:DocDepends},
 ${maven:DocOptionalDepends}
Suggests:
 libjboss-xnio-java
Description: Documentation for jboss-xnio
 XNIO is a simplified low-level I/O layer which can be used anywhere you are
 using non-blocking I/O today. It frees you from the hassle of dealing with
 Selectors and the lack of NIO support for multicast sockets and non-socket
 I/O, while still maintaining all the capabilities present in NIO, and it opens
 the door to non-obvious optimizations.
 .
 This package contains the API documentation of libjboss-xnio-java.
